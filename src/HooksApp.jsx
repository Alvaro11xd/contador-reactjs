import React from "react";
import ContadorComponent from "./components/ContadorComponent";

export default function HooksApp() {
  return (
    <div>
      <h1>App de Hooks</h1>
      <ContadorComponent />
    </div>
  );
}
