import React, { useState } from "react";

export default function useCounter(initialValue = 0) {
  const [counter, setCounter] = useState(initialValue);
  const increment = (val = 1) => setCounter(counter + val);
  const reset = () => setCounter(0);
  const decrement = (val = 1, negativo = true) => {
    if (!negativo && counter - val < 0) {
      setCounter(0);
      return;
    }
    setCounter(counter - val);
  };
  return {
    counter,
    increment,
    reset,
    decrement,
  };
}
