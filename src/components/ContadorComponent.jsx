import useCounter from "../hooks/useCounter.js";

export default function ContadorComponent() {
  const { counter, increment, reset, decrement } = useCounter(0);

  return (
    <>
      <h1>Contador: {counter}</h1>
      <div className="d-flex gap-2 ">
        <button className="btn btn-primary" onClick={() => increment(1)}>
          +1
        </button>
        <button
          className="btn btn-warning text-capitalize "
          onClick={() => reset()}>
          reset
        </button>
        <button
          className="btn btn-secondary"
          onClick={() => decrement(1, false)}>
          -1
        </button>
      </div>
    </>
  );
}
